import React, { Component } from 'react';
import moment from "moment";

import 'normalize.css';
import './App.css';

import Header from './components/Header';
import Settings from './components/Settings';
import TicketList from './components/TicketList/TicketList';

import tickets from './services/tickets.js';
import { sortTicket } from './utils';

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
        tickets:[],
        currency: "RUB",
        transfers: [],
        sortedTickets:[]
    }
  }

  componentDidMount() {
    const formatDate = (date) => moment(date, "DD.MM.YY").format("D MMM YYYY, ddd");
    const ticketList = tickets.tickets
      .sort((first, second) => (first.price - second.price))
      .map((item, index) => {   
        return {
          id: index.toString(),
          companyName: item.carrier,
          from: {
            airport: `${ item.origin }, ${ item.origin_name }`,
            date: formatDate(item.departure_date),
            time: item.departure_time,
          },
          to: {
            airport: `${ item.destination_name }, ${ item.destination }`,
            date: formatDate(item.arrival_date),
            time: item.arrival_time,
          },
          price: item.price,
          transfer: item.stops
        };			
      });


    this.setState({ 
       tickets:ticketList,
       currency:this.state.currency
     });
  }

  handleStopsChange = (event) => {
    const transfer = parseInt(event.target.value, 10),
          transfers = this.state.transfers,
          tickets = this.state.tickets,
          transferIndex = transfers.indexOf(transfer);

    if(transfers === -1) {
      console.log()
      this.setState({ 
        sortedTickets: transfers.sort(), 
        transfers: transfers
      });
      return;
    }
    
    transferIndex === -1 ? transfers.push(transfer) : transfers.splice(transferIndex, 1);
    
    let filteredTickets = [];
    if(transfers){      
      transfers
        .sort()
        .forEach((stop)=>{
          filteredTickets.push(sortTicket(stop, tickets));
        })
      filteredTickets = [].concat(...filteredTickets);
    }

    this.setState({ 
      sortedTickets: filteredTickets, 
      transfers: transfers
    });
  }
  
  handleStopsOnly = (event) => {
    let filteredTickets = [].concat(...sortTicket(parseInt(event.target.dataset.value, 10), this.state.tickets));
    this.setState({ 
      sortedTickets: filteredTickets
    });
  }

	handleCurrencyChange = (event) => {
    const currency = event.target.value;
		this.setState({
      currency: currency
    });
	}

  render() {
    const ticketsRendering = this.state.sortedTickets.length ?  this.state.sortedTickets :  this.state.tickets;
    return (
      <div className="app">
        <Header />
        <main className='app__content'>
          <Settings
            currency={ this.state.currency }
            handleStopsOnly={ this.handleStopsOnly }
            handleStopsChange={ this.handleStopsChange } 
            handleCurrencyChange={ this.handleCurrencyChange } />
          <TicketList
           tickets={ ticketsRendering } 
           currency={ this.state.currency }/>
        </main>
      </div>
    );
  }
}

export default App;
