import React from 'react';
import PropTypes from 'prop-types';

import Currency from './Currency/Currency';
import StopFilters from './StopFilters';

import '../style/settings.css';


const Settings = ({currency, handleStopsOnly, handleCurrencyChange, handleStopsChange}) => {
  return (
    <div className="settings">
      <Currency
        currency={ currency }
        handleCurrencyChange={ handleCurrencyChange }/>
      <StopFilters 
        handleStopsChange={ handleStopsChange }
        handleStopsOnly={ handleStopsOnly }/>
    </div>
  );
}

Settings.propTypes = {
  currency: PropTypes.string.isRequired,
  handleStopsOnly: PropTypes.func.isRequired,
  handleCurrencyChange: PropTypes.func.isRequired,
  handleStopsChange: PropTypes.func.isRequired
};

export default Settings;
