import React, { Fragment } from 'react';
import PropTypes from 'prop-types';

import '../style/filter.css'

const StopElement = ({ handleStopsChange, handleStopsOnly, description }) => {
    return (
        <Fragment>
            <input 
                onChange={ handleStopsChange }
                id={ description.id }
                type='checkbox' 
                value={ description.value } />
            <label htmlFor={ description.id }>
                { description.label }
            </label>
            <p 
                className="filter__stopOnly"
                onClick={ handleStopsOnly }
                data-value={ description.value }>
            ТОЛЬКО
            </p>
        </Fragment> 
    ) 
}

StopElement.propTypes = {
    handleStopsOnly: PropTypes.func.isRequired,
    handleStopsChange: PropTypes.func.isRequired,
    description: PropTypes.shape({
		id: PropTypes.string.isRequired,
        value: PropTypes.number.isRequired,
		label: PropTypes.string.isRequired
	}),
};
export default StopElement;