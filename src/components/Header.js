import React from 'react';

import '../style/header.css';

const Header = () => {
    return (
        <header className="header">
            <a className="header__link" href="/">
            <img
                src="assets/logo.png"
                alt="logo" />
            </a>
		</header>
    );
}; 

export default Header;