import React from 'react';
import PropTypes from 'prop-types';

const CurrencyElement = ({selected, currency, clickHandler}) => {
	const selectedElement = selected.index === selected.selected ? "selected" : ""
	return (
		<button
		onClick={clickHandler}
		value={currency}
		className={selectedElement}>
		{currency}
		</button>
	);
}; 

CurrencyElement.propTypes = {
	selected: PropTypes.shape({
		index: PropTypes.number.isRequired,
		selected: PropTypes.number.isRequired
	}),
	currency: PropTypes.string.isRequired,
	clickHandler: PropTypes.func.isRequired
}
export default CurrencyElement;