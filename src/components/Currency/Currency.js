import React from 'react';
import PropTypes from 'prop-types';

import CurrencyElement from './CurrencyElement';

import currencyService from './../../services/currency';
import './../../style/currency.css'


class Currency extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            currency: ["RUB"],
            selected: 0
        }
    }

    componentDidMount() {
		currencyService.fetchRates()
			.then(() => {
                const currencies = currencyService.getSupportedCurrencies();
                const currencyList = [
                    ...this.state.currency,
                    ...currencies
                ]
				this.setState({ 
					currency: currencyList
				});
			})
			.catch((err) => { 
				console.log(err);
			});
    }

	render() {
		return ( 
			<div className="currency">
				<div className="currency__header">
					Валюта
				</div>
				<div className="currency__list">
					{
                        this.state.currency.map((currency, index) => (
                            <CurrencyElement
                                selected={ {index:index, selected: this.state.currency.indexOf(this.props.currency)} }   
                                key={index}
                                currency={currency} 
                                clickHandler={this.props.handleCurrencyChange}/>
                        )) 
                    }
				</div>
			</div>
		);
	}
}
Currency.propTypes = {
    currency: PropTypes.string.isRequired,
    handleCurrencyChange: PropTypes.func.isRequired
}   
export default Currency;