import React from 'react';
import PropTypes from 'prop-types';

import '../style/filter.css';
import StopElelemt from './StopElement';

class StopFilters extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            stopsElem: [{
                id: 'withOutTransfer',
                value: 0,
                label: 'Без пересадок'
            }, {
                id: 'oneTransfer',
                value: 1,
                label: '1 пересадка'
            }, {
                id: 'twoTransfers',
                value: 2,
                label: '2 пересадки'
            }, {
                id: 'threeTranfers',
                value: 3,
                label: '3 пересадки'
            }]
        }
    }
    
    render() {
		return ( 
            <div className="filter">
                <div className="filter__header">
                    Количество пересадок
                </div>
                <div className="filter__list">
                    <ul>
                        <li className="filter__item">
                            <input 
                                onChange={ this.props.handleStopsChange } 
                                value={ -1 }
                                id='all' 
                                type='checkbox'/>
                            <label htmlFor='all'>Все</label>
                        </li>
                        {
                            this.state.stopsElem.map((elem, index) => {
                                return (
                                    <li 
                                        key = { index }
                                        className="filter__item">
                                        <StopElelemt
                                            handleStopsChange={ this.props.handleStopsChange }
                                            handleStopsOnly={ this.props.handleStopsOnly }
                                            description={ elem }/>
                                    </li>
                                )
                            })
                        }
                    </ul>			
                </div>
            </div>
        )
    }
}

StopFilters.propTypes = {
    handleStopsOnly: PropTypes.func.isRequired,
    handleStopsChange: PropTypes.func.isRequired
};

export default StopFilters;