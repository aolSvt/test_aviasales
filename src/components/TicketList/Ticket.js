import React from 'react';
import PropTypes from 'prop-types';

import { formatPrice, formatTransfer } from '../../utils';
import currencyService from '../../services/currency';

import '../../style/ticket.css'

const Ticket = ({ info, currency }) => {
    const price = currencyService.exchange(info.price, currency);
    return (
        <div className="ticket">
            <section className="ticket__company">
                <img
                src={ `assets/${info.companyName}.png` }
                width="115px"
                alt={ info.companyName } />
                <button>
                    <span>Купить</span>
                    <span>за { formatPrice(price, currency) }</span>
                </button>
            </section>
            <section className="ticket__details">
                <div className="ticket__details-from">
                    <p className="ticket__details-time">{ info.from.time }</p>
                    <p className="ticket__details-airport">{ info.from.airport }</p>
                    <p className="ticket__details-date">{ info.from.date }</p>
                </div>
                <div className="ticket__details-transfer">
                    <p className="ticket__details-stops">{ formatTransfer(info.transfer) } &nbsp;</p>
                    <div className="ticket__details-path">
							<div className="ticket__details-line"></div>
							<div className="ticket__details-airplane"></div>
						</div>
                </div>
                <div className="ticket__details-to">
                    <p className="ticket__details-time">{ info.to.time }</p>
                    <p className="ticket__details-airport">{ info.to.airport }</p>
                    <p className="ticket__details-date">{ info.to.date }</p>
                </div>
            </section>
        </div>
    );
}; 

Ticket.propTypes = {
	info: PropTypes.shape({
        id: PropTypes.string.isRequired,
        companyName: PropTypes.string.isRequired,
		price: PropTypes.number.isRequired,
		transfer: PropTypes.number.isRequired,
		from: PropTypes.shape({
			airport: PropTypes.string.isRequired,
			date: PropTypes.string.isRequired,
			time: PropTypes.string.isRequired,
		}),
		to: PropTypes.shape({
			airport: PropTypes.string.isRequired,
			date: PropTypes.string.isRequired,
			time: PropTypes.string.isRequired,
		})
	}),
	currency: PropTypes.string.isRequired
};

export default Ticket;
