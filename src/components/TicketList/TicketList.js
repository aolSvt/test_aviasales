import React from 'react';
import PropTypes from 'prop-types';

import Ticket from './Ticket';
import '../../style/ticketList.css'

const TicketList = ({ tickets, currency }) => {
    return (
        <div className="ticketList">
            {tickets.map((info) => ( 
                        <Ticket
                            key={ info.id }
                            info={ info }
                            currency={ currency }/> 
                    ))}
        </div>
    );
}; 

TicketList.propTypes = {
    tickets: PropTypes.arrayOf(PropTypes.object).isRequired,
    currency: PropTypes.string.isRequired
};

export default TicketList;