export function formatTransfer(stops) {
	if (stops < 1) {
		return "прямой";
	}
	if (stops === 1) {
		return "1 пересадка"; 
	}
	if (stops > 1 && stops < 5) {
		return `${stops} пересадки`;
	}
	return `${stops} пересадок`;
}

const currencyFormatters = {
	"RUB": (price) => `${price}₽`,
	"USD": (price) => `$${price}`,
	"EUR": (price) => `€${price}`
}

function pricePrettier(price) {
	return price.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1 ")
}

export function formatPrice(price, currency) {
	if (!price) {
		return "";
	}
	const formattedPrice = pricePrettier(price);
	const formatter = currencyFormatters[currency];
	return formatter ? formatter(formattedPrice) : `${formattedPrice} ${currency}`;
}

export function sortTicket(transfer, tickets) {
	let result = tickets.filter((item) => {
		return item.transfer === transfer
	});
	return result;
}