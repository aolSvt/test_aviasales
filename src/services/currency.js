const currencyApiEndpoint = "http://free.currencyconverterapi.com/api/v5";


const baseCurrency = "RUB";
const supportedCurrencies = [ "EUR", "USD" ]

class CurrencyService {

	constructor() {
		this.convertion = {
			'EUR': null,
			'USD': null,
			'RUB': 1
		};
	}

	async fetchRates() {

		const query = supportedCurrencies
						.map((c) => (`${baseCurrency}_${c}`))
						.join();

		const request = `${currencyApiEndpoint}/convert?q=${query}`;

		const response = await fetch(request);
		if (response.ok) {
			const { results } = await response.json();
			Object.keys(results).forEach(res => {
				this.convertion[results[res].to] = results[res].val;
			});
		}
		
	}


	exchange(price, currency) {
		const rate = this.convertion[currency];
		if (!rate) {
			return null;
		}
		const result = parseFloat(price);
		if (!result || isNaN(result)) {
			return null;
		}
		return Math.ceil(result * rate);
	}

	getRates() {
		return this.rates;
	}

	getSupportedCurrencies() {
		return supportedCurrencies;
	}

	resetRates() {
		this.rates = {};
		this.rates[baseCurrency] = 1;
	}
}

export default new CurrencyService();