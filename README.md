# Тестовое задание Aviasales (frontend)

## Задачи
 (#task1) Рендер билетов  - **done**
 (#task2) Фильтрация билетов - **done**
 (#task3) Переключение валюты - **done**
 (#task4) Верстка билета, фильтра, переключения валют - **done**
 (#task5) Респонсивность до 320px на ваш выбор - **done**
 

 **Описание** [задания](https://github.com/KosyanMedia/test-tasks/tree/master/aviasales)
 
## Запуск 
Приложение будет доступно по адресу localhost:3000   
npm install  
npm start  
